<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ClientsController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\InvoiceController;
use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Route;
Route::get('/', function () {
    return view('welcome');
});


Route::get('/products', [ProductController::class, 'index'])->name('ProductsView');
Route::get('/product/{id}/delete', [ProductController::class, 'delete'])->name('deleteProduct');
Route::get('/products/form', [ProductController::class, 'form'])->name('ProductsForm');
Route::post('/products/insert', [ProductController::class, 'insert'])->name('insertProduct');

Route::get('/clients', [ClientsController::class, 'clientsview'])->name('ClientsView');
Route::get('/clients/{id}/delete', [ClientsController::class, 'delete'])->name('deleteClient');
Route::get('/clients/form', [ClientsController::class, 'form'])->name('ClientsForm');
Route::post('/clients/insert',  [ClientsController::class, 'insert'])->name('insertClient');

Route::get('/invoices/form', [InvoiceController::class, 'form'])->name('InvoicesForm');
Route::post('/invoices/insert',  [InvoiceController::class, 'insert'])->name('insertInvoice');
Route::get('/invoices', [InvoiceController::class, 'index'])->name('InvoicesView');

Route::get('/categories', [CategoryController::class, 'index'])->name('CategoriesView');
Route::get('/categories/{category}/delete', [CategoryController::class, 'delete'])->name('deleteCategory');
Route::get('/category/form', [CategoryController::class, 'form'])->name('CategoryForm');
Route::post('/category/insert',  [CategoryController::class, 'insert'])->name('insertCategory');

Route::get('/comments', [CommentController::class, 'index'])->name('CommentsView');
Route::get('/comments/form', [CommentController::class, 'form'])->name('CommentsForm');
Route::post('/comments/insert',  [CommentController::class, 'insert'])->name('insertComment');
