<style>
    /* Estilo para cada comentario */
    .comment-container {
        margin-bottom: 30px;
        border: 1px solid #ccc;
        padding: 20px;
        border-radius: 10px;
        background-color: #f9f9f9;
        box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
        transition: all 0.3s ease-in-out;
    }

    .comment-container:hover {
        transform: scale(101%);
        box-shadow: 0 6px 12px rgba(0, 0, 0, 0.15);
    }

    .comment-info {
        font-weight: bold;
        margin-bottom: 8px;
        color: #333;
    }

    .comment-text {
        margin-bottom: 15px;
        color: #666;
    }
</style>

@foreach($comments as $comment)
    <div class="comment-container">
        <div class="comment-info">ID: {{ $comment->id }}</div>
        <div class="comment-info">Client: {{ $comment->client->name }}</div>
        <div class="comment-text">Comment: {{ $comment->comment }}</div>
        <div class="comment-info">Date: {{ $comment->created_at }}</div>
    </div>
@endforeach
