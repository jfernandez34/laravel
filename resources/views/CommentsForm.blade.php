<style>
    .contingut {
        margin: 20px;
        padding: 20px;
        border: 1px solid #ccc;
        border-radius: 5px;
        background-color: #f9f9f9;
    }

    .contingut h1 {
        font-size: 24px;
        color: #333;
    }

    .contingut form {
        margin-top: 20px;
    }

    .contingut label {
        display: block;
        margin-bottom: 10px;
        font-weight: bold;
        color: #666;
    }

    .contingut select,
    .contingut textarea {
        width: 100%;
        padding: 10px;
        margin-bottom: 10px;
        border: 1px solid #ccc;
        border-radius: 5px;
        font-size: 16px;
    }

    .contingut button {
        padding: 10px 20px;
        background-color: #007bff;
        color: #fff;
        border: none;
        border-radius: 5px;
        font-size: 18px;
        cursor: pointer;
    }

    .contingut button:hover {
        background-color: #0056b3;
    }
</style>

<div class="contingut">
    <h1>Comentar:</h1>
    <form method="POST" action="{{ route('insertComment') }}">
        @csrf
        <label>Client:</label>
        <select id="client_id" name="client_id">
            @foreach($clients as $client)
                <option value="{{ $client->id }}">{{ $client->name }}</option>
            @endforeach
        </select>
        <br><br>
        <label>Commentari:</label>
        <textarea name="comment" id="comment"></textarea><br>
        <button type="submit">Publicar</button>
    </form>
</div>
