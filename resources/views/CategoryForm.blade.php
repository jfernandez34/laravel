<style>
    .contingut {
        max-width: 400px;
        margin: 0 auto;
        padding: 20px;
        border: 1px solid #ccc;
        border-radius: 5px;
        background-color: #f9f9f9;
    }

    .contingut h1 {
        font-size: 24px;
        text-align: center;
        margin-bottom: 20px;
    }

    .contingut form {
        display: flex;
        flex-direction: column;
    }

    .contingut label {
        font-weight: bold;
        margin-bottom: 10px;
    }

    .contingut input {
        padding: 8px;
        margin-bottom: 15px;
        border: 1px solid #ccc;
        border-radius: 3px;
    }

    .contingut button {
        padding: 10px 20px;
        background-color: #007bff;
        color: #fff;
        border: none;
        border-radius: 5px;
        cursor: pointer;
        transition: background-color 0.3s ease;
    }

    .contingut button:hover {
        background-color: #0056b3;
    }
</style>

<div class="contingut">
    <h1>Afegir categoria:</h1>
    <form method="POST" action="{{route('insertCategory')}}">
        @csrf
        <label>Nom: </label><input name="name" placeholder="Cetegoria"><br><br>
        <label>IVA: </label><input name="percent" placeholder="IVA"><br><br>
        <button type="submit">Afegir categoria</button>
    </form>
</div>
