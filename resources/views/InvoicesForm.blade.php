<style>

    h1 {
        text-align: center;
        font-size: 24px;
        margin-bottom: 20px;
    }

    .contingut {
        max-width: 600px;
        margin: 0 auto;
        padding: 20px;
        border: 1px solid #ccc;
        border-radius: 5px;
        background-color: #f9f9f9;
    }

    label {
        display: block;
        margin-bottom: 5px;
    }

    select, input {
        width: 100%;
        padding: 8px;
        margin-bottom: 10px;
        border: 1px solid #ccc;
        border-radius: 5px;
        box-sizing: border-box;
    }

    button.comprar {
        width: 100%;
        padding: 10px;
        background-color: #4CAF50;
        color: white;
        border: none;
        border-radius: 5px;
        cursor: pointer;
        transition: background-color 0.3s ease;
    }

    button.comprar:hover {
        background-color: #45a049;
    }

    p.error {
        color: #ff0000;
        margin-top: 10px;
    }

</style>

<div class="contingut">
    <h1>Comprar producto:</h1>
    <form method="POST" action="{{route('insertInvoice')}}">
        @csrf
        <label>Cliente: </label>
        <select name="client">
            @foreach($clients as $client)
                <option value="{{$client->id}}">{{$client->name}}</option>
            @endforeach
        </select>
        <br><br>
        <label>Producto: </label>
        <select name="product">
            @foreach($products as $product)
                <option value="{{$product->id}}">{{$product->name}}</option>
            @endforeach
        </select>
        <label>Cantidad: </label>
        <input type="number" id="quantity" name="quantity">
        <br><br>
        <button class="comprar" type="submit">Comprar</button>
        @if($error)
            <p class="error">No quedan productos suficientes.</p>
        @endif
    </form>
</div>
