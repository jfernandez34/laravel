<style>
    /* Estilo para el encabezado de la tabla */
    h1 {
        text-align: center;
        font-size: 24px;
        margin-bottom: 20px;
    }

    /* Estilo para la tabla */
    table {
        width: 100%;
        border-collapse: collapse;
        margin-bottom: 20px;
    }

    th, td {
        padding: 12px;
        text-align: center;
        border-bottom: 1px solid #ddd;
    }

    th {
        background-color: #f2f2f2;
    }

    /* Estilo para el botón de eliminación */
    button {
        margin-top: 10px;
        display: block;
        margin: auto;
        padding: 8px 16px;
        background-color: #ff4444;
        color: #fff;
        border: none;
        border-radius: 5px;
        cursor: pointer;
        transition: background-color 0.3s ease;
    }

    button:hover {
        background-color: #cc0000;
    }

    /* Estilo para el botón de agregar productos */
    .add-button {
        margin-top: 20px;
        display: block;
        margin: auto;
        padding: 8px 16px;
        background-color: #00cc00;
        color: #fff;
        border: none;
        border-radius: 5px;
        cursor: pointer;
        transition: background-color 0.3s ease;
    }

    .add-button:hover {
        background-color: #008800;
    }
</style>

<h1>Tabla:</h1>
<div class="container mx-auto">
    <table>
        <thead>
        <tr>
            <th>Nombre</th>
            <th>Cantidad</th>
            <th>Precio</th>
            <th>ID de Categoría</th>
            <th>Acción</th>
        </tr>
        </thead>
        <tbody>
        @foreach($products as $product)
            <tr>
                <td>{{$product->name}}</td>
                <td>{{$product->quantity}}</td>
                <td>{{$product->price}}</td>
                <td>{{$product->category_id}}</td>
                <td><button onclick="window.location='{{url("/product/$product->id/delete")}}'">Delete</button></td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <button class="add-button" onclick="window.location='{{url("/products/form")}}'">Afegeix productes!</button>
</div>
