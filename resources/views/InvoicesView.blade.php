<style>
    h1 {
        text-align: center;
        margin-bottom: 20px;
        color: #333;
    }

    table {
        width: 80%;
        margin: auto;
        border-collapse: collapse;
        border: 2px solid #ddd;
        border-radius: 10px;
    }

    th, td {
        padding: 10px;
        text-align: left;
    }

    th {
        background-color: #f2f2f2;
        border-bottom: 2px solid #ddd;
    }

    tr:nth-child(even) {
        background-color: #f9f9f9;
    }

    tr:hover {
        background-color: #f2f2f2;
    }
</style>

<h1>Facturas</h1>
<table>
    <thead>
    <tr>
        <th>ID</th>
        <th>Cliente</th>
        <th>Precio</th>
        <th>Precio con IVA</th>
        <th>Fecha</th>
    </tr>
    </thead>
    <tbody>
    @foreach($invoices as $invoice)
        <tr>
            <td>{{ $invoice->id }}</td>
            <td>{{ $invoice->client_id }}</td>
            <td>{{ $invoice->price }}</td>
            <td>{{ $invoice->price_iva }}</td>
            <td>{{ $invoice->data }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
