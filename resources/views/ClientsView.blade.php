<style>
    /* Estilo para el encabezado de la tabla */
    h1 {
        text-align: center;
        font-size: 24px;
        margin-bottom: 20px;
    }

    /* Estilo para cada fila de la tabla */
    .container {
        max-width: 800px;
        margin: 0 auto;
    }

    .flex {
        display: flex;
        align-items: center;
        justify-content: space-between;
        border-bottom: 1px solid #ddd;
        padding: 10px 0;
    }

    .flex:last-child {
        border-bottom: none;
    }

    /* Estilo para cada celda de la fila */
    .flex > div {
        width: calc(100% / 3);
        text-align: center;
    }

    /* Estilo para el botón de eliminación */
    button {
        margin-top: 10px;
        padding: 8px 16px;
        background-color: #ff4444;
        color: #fff;
        border: none;
        border-radius: 5px;
        cursor: pointer;
        transition: background-color 0.3s ease;
    }

    button:hover {
        background-color: #cc0000;
    }
</style>

<h1>Tabla:</h1>
<div class="container mx-auto">
    <!-- Nothing in life is to be feared, it is only to be understood. Now is the time to understand more, so that we may fear less. - Marie Curie -->
    @foreach($clients as $client)
        <div class="flex bg-white">
            <div class="w-1/4 p-4 justify-center">{{$client->name}}</div>
            <div class="w-1/4 p-4 justify-center">{{$client->email}}</div>
            <div class="w-1/4 p-4 justify-center">{{$client->phone}}</div>
        </div>
        <button onclick="window.location='{{url("/clients/$client->id/delete")}}'">Delete</button>
    @endforeach
</div>
