<style>
    .contingut {
        width: 70%;
        margin: auto;
        padding: 20px;
        border: 1px solid #dddddd;
        border-radius: 5px;
        background-color: #f9f9f9;
    }

    .contingut h1 {
        color: #333;
        margin-bottom: 20px;
    }

    .contingut table {
        width: 100%;
        border-collapse: collapse;
        margin-bottom: 20px;
    }

    .contingut th,
    .contingut td {
        padding: 10px;
        text-align: left;
        border-bottom: 1px solid #dddddd;
    }

    .contingut th {
        background-color: #f2f2f2;
        font-weight: bold;
    }

    .contingut button {
        padding: 8px 16px;
        background-color: #007bff;
        color: white;
        border: none;
        border-radius: 3px;
        cursor: pointer;
    }

    .contingut .afegir {
        margin-top: 20px;
    }
</style>

<div class="contingut">
    <h1>Categorias:</h1>
    <table>
        <thead>
        <tr>
            <th>Nombre</th>
            <th>IVA</th>
            <th>Borrar</th>
        </tr>
        </thead>
        <tbody>
        @foreach($categories as $category)
            <tr>
                <td>{{$category->name}}</td>
                <td>{{$category->percent}}</td>
                <td><button type="button" onclick="location.href='{{url('/categories/'.$category->id.'/delete')}}'">Eliminar</button></td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <button class="afegir" type="button" onclick="location.href='{{url('/category/form')}}'">Añadir categorias</button>
</div>
