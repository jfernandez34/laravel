<style>
    /* Estilos para el formulario */
    .form-container {
        width: 50%;
        margin: auto; /* Centrar el formulario */
        padding: 20px;
        border: 1px solid #dddddd;
        border-radius: 5px;
        background-color: #f9f9f9;
    }

    .form-container label {
        font-weight: bold;
    }

    .form-container input {
        width: 100%;
        padding: 8px;
        margin-bottom: 10px;
        border: 1px solid #ccc;
        border-radius: 3px;
        box-sizing: border-box;
    }

    .form-container button {
        padding: 10px 20px;
        background-color: #4CAF50;
        color: white;
        border: none;
        border-radius: 3px;
        cursor: pointer;
    }

    /* Estilos para el botón de volver */
    .back-button {
        margin-top: 20px;
    }

    .back-button button {
        padding: 8px 16px;
        background-color: #007bff;
        color: white;
        border: none;
        border-radius: 3px;
        cursor: pointer;
    }
</style>

<div class="form-container">
    <form method="POST" action="{{ route('insertClient') }}">
        @csrf
        <label for="name">Nombre:</label><br>
        <input id="name" name="name" required><br>
        <label for="email">Correo electrónico:</label><br>
        <input id="email" name="email" type="email" required><br>
        <label for="phone">Teléfono:</label><br>
        <input id="phone" name="phone" type="tel" required><br>
        <button type="submit">Crear</button>
    </form>

    <div class="back-button">
        <label>Volver</label><br>
        <button onclick="location.href='{{ url('/clients') }}'">Atrás</button>
    </div>
</div>
