<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class invoices_seeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('invoices')->insert([
            ["client_id" => 1, "price" => 100.00, "price_iva" => 121.00, "data" => '2024-06-01'],
            ["client_id" => 1, "price" => 150.00, "price_iva" => 181.50, "data" => '2024-06-02'],
            ["client_id" => 1, "price" => 200.00, "price_iva" => 242.00, "data" => '2024-06-03'],
        ]);
    }
}
