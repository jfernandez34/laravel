<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class comments_seeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('comments')->insert([
            ["client_id" => 1, "comment" => "Comentario de cliente 1"],
            ["client_id" => 1, "comment" => "Comentario de cliente 2"],
            ["client_id" => 1, "comment" => "Comentario de cliente 3"],
        ]);
    }
}
