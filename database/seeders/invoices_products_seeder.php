<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class invoices_products_seeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('invoices_products')->insert([
            ["invoice_id" => 1, "product_id" => 1, "price" => 50.00, "quantity" => 2, "iva" => 10],
            ["invoice_id" => 1, "product_id" => 2, "price" => 75.00, "quantity" => 1, "iva" => 15],
            ["invoice_id" => 2, "product_id" => 3, "price" => 100.00, "quantity" => 3, "iva" => 20],
        ]);
    }
}
