<?php

namespace Database\Seeders;

use App\Models\invoices_products;
// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->call([
            clients_seeder::class,
            comments_seeder::class,
            categories_seeder::class,
            invoices_products_seeder::class,
            invoices_seeder::class,
            products_seeder::class,
        ]);
    }
}
