<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class products_seeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('products')->insert([
            ["category_id" => 1, "name" => "Producto 1", "price" => 100, "quantity" => 5],
            ["category_id" => 2, "name" => "Producto 2", "price" => 150, "quantity" => 3],
            ["category_id" => 3, "name" => "Producto 3", "price" => 200, "quantity" => 7],
        ]);
    }
}
