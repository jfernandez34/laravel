<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class clients_seeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('clients')->insert([
            ["name" => "Ruben", "email" => "rcollado@ies-sabadell.cat", "phone" => "652928423"],
            ["name" => "Jluis", "email" => "jluis@ies-sabadell.cat", "phone" => "651238423"],
            ["name" => "Ariel", "email" => "ariel@ies-sabadell.cat", "phone" => "659650932"],
        ]);
    }
}
