<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class categories_seeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('categories')->insert([
            ["name" => "Categoria 1", "percent" => 10],
            ["name" => "Categoria 2", "percent" => 15],
            ["name" => "Categoria 3", "percent" => 20],
        ]);
    }
}
