<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index(){
        $categories = Category::all();
        return view('CategoryView')->with('categories', $categories);
    }

    public function delete(Category $category){
        $category->delete();
        return redirect('/categories');
    }

    public function insert(Request $request){
        $category = new Category();
        $category->name = $request->input('name');
        $category->percent = $request->input('percent');
        $category->save();
        return redirect('/categories');
    }

    public function form(){
        return view('CategoryForm');
    }
}
