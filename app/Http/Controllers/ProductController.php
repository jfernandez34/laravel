<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use function Laravel\Prompts\alert;

class ProductController extends Controller
{
    function index()
    {
        $products = Product::all();
        return view('ProductsView')->with('products',$products);
    }
    function delete($id)
    {
        $product = Product::find($id);
        $product->delete();
        return redirect('/products');
    }
    function form(Request $request)
    {
        $categories = Category::all();
        return view('ProductsForm')->with('categories',$categories);
    }
    function insert(Request $request)
    {
        $products = new Product();
        $products->name = $request->input('name');
        $products->price = $request->input('price');
        $products->quantity = $request->input('quantity');
        $products->category_id = $request->input('category_id');

        $products->save();
        return redirect('/products');
    }
}
