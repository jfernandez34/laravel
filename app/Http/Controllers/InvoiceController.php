<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Invoice;
use App\Models\Invoices_Products;
use App\Models\Product;
use App\Models\Clients;
use Carbon\Carbon;
use Illuminate\Http\Request;

class InvoiceController extends Controller
{
    public function form(){
        $clients = Clients::all();
        $products = Product::all();
        $error = false;
        return view('InvoicesForm')->with('clients', $clients)->with('products', $products)->with('error', $error);
    }

    public function index(){
        $invoices = Invoice::all();
        return view('InvoicesView')->with('invoices',$invoices );
    }

    public function insert(Request $request){
        $products = Product::find($request->input('product'));
        $category = Category::find($products->category_id);
        if($request->input('quantity') == null || $products->quantity < $request->input('quantity') || 0 >= $request->input('quantity')){
            $error = true;
            $clients = Clients::all();
            $products = Product::all();

            return view('InvoicesForm')->with('clients', $clients)->with('products', $products)->with('error', $error);
        } else {
            $invoice = new Invoice();
            $invoice->client_id = $request->input('client');
            $invoice->data = Carbon::now()->toDate();
            $invoice->price = $products->price*$request->input('quantity');
            $invoice->price_iva = $products->price*$request->input('quantity')+($products->price*$request->input('quantity')*$category->percent/100);
            $invoice->save();

            $products->quantity = $products->quantity - $request->input('quantity');
            $products->save();

            $invoice_product = new invoices_products();
            $invoice_product->invoice_id = $invoice->id;
            $invoice_product->product_id = $products->id;
            $invoice_product->price = $products->price;
            $invoice_product->iva = $category->percent;
            $invoice_product->quantity = $request->input('quantity');
            $invoice_product->save();

            return redirect('/invoices');
        }
    }
}
