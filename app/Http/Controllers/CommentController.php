<?php

namespace App\Http\Controllers;

use App\Models\Clients;
use App\Models\Comments;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function index(){
        $comments = Comments::with('client')->get();
        return view('CommentsView')->with('comments',$comments );
    }
    public function form(Request $request){
        $clients = Clients::all();
        return view('CommentsForm',['clients'=>$clients]);
    }

    public function insert(Request $request){
        $comment = new Comments();
        $comment->client_id = $request->input('client_id');
        $comment->comment = $request->input('comment');
        $comment->save();
        return redirect()->route('CommentsView');
    }
}
