<?php

namespace App\Http\Controllers;

use App\Models\Clients;
use App\Models\User;
use App\Http\Models\Client;
use Illuminate\Http\Request;

class ClientsController extends Controller
{
    function clientsview()
    {
        $clients = Clients::all();
        return view('ClientsView')->with('clients',$clients);
    }
    function delete($id)
    {
        $client = Clients::find($id);
        $client->delete();

        return redirect('/clients');
    }
    function form(Request $request)
    {
        return view('ClientsForm');
    }
    function insert(Request $request)
    {

        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|max:99999',
            'phone' => 'required|max:255'
        ]);
        $client = new Clients();
        $client->name = $request->input('name');
        $client->email = $request->input('email');
        $client->phone = $request->input('phone');

        $client->save();
        return redirect('/clients');
    }
}
